/**
 * Created by fasolens on 18.07.17.
 */
import java.util.ArrayList;

public class Portal {

    private ArrayList<String> polaPolozenia;
    // int iloscTrafien; not need it anymore

    public void setPolaPolozenia(ArrayList<String> polaPolozenia) {
        this.polaPolozenia = polaPolozenia;
    }

    public String sprawdz(String ruch) {
        String wynik = "pudło";

        int Index = polaPolozenia.indexOf(ruch);

        if (Index >= 0) {
            polaPolozenia.remove(index);

            if (polaPolozenia.isEmpty()) {
                wynik = "zatopiony";
            } else {
                wynik = "trafiony";
            } // end of if
        } // end of if

        return wynik;
    } // end of method sprawdz
} // end of class Portal
